import Layout from '../components/layout';

const About = () => (
    <Layout>
        <div>
            <h1>About demoApp on NextJS</h1>
            <p>Some demoApp on NextJS that gets the current price of Bitcoin</p>
        </div>
    </Layout>
);

export default About;